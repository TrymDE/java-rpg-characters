package no.noroff.accelerate.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    /**
     * Tests whether newly created character is level 1.
     */
    @Test
    void getLevel_onCreate_shouldBe1() {
        // Arrange
        int expected = 1;

        // Act
        Mage testCharacter = new Mage("Test character");
        int actual = testCharacter.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether a character that has leveled up once is level 2
     */
    @Test
    void levelUp_onFirstCall_shouldBeLevel2() {
        // Arrange
        int expected = 2;

        // Act
        Mage testCharacter = new Mage("Test character");
        testCharacter.levelUp();
        int actual = testCharacter.getLevel();

        // Assert
        assertEquals(expected, actual);
    }

    // For all attribute tests: converts attributes to string for better feedback on test fail.

    /**
     * Tests whether Mage is created with the correct attributes
     */
    @Test
    void getTotalAttributes_onCreateMage_shouldBeMageBaseAttributes() {
        // Arrange
        String expected = "1, 1, 8";

        // Act
        Mage testCharacter = new Mage("Test character");
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether Ranger is created with the correct attributes
     */
    @Test
    void getTotalAttributes_onCreateRanger_shouldBeRangerBaseAttributes() {
        // Arrange
        String expected = "1, 7, 1";

        // Act
        Ranger testCharacter = new Ranger("Test character");
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether Rogue is created with the correct attributes
     */
    @Test
    void getTotalAttributes_onCreateRogue_shouldBeRogueBaseAttributes() {
        // Arrange
        String expected = "2, 6, 1";

        // Act
        Rogue testCharacter = new Rogue("Test character");
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether Warrior is created with the correct attributes
     */
    @Test
    void getTotalAttributes_onCreateWarrior_shouldBeWarriorBaseAttributes() {
        // Arrange
        String expected = "5, 2, 1";

        // Act
        Warrior testCharacter = new Warrior("Test character");
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether Mage gains the correct attributes on levelup
     */
    @Test
    void getTotalAttributes_afterMageLevelUp_shouldBeMageBaseAttributesPlusLevelAttributes() {
        // Arrange
        String expected = "2, 2, 13";

        // Act
        Mage testCharacter = new Mage("Test character");
        testCharacter.levelUp();
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether Ranger gains the correct attributes on levelup
     */
    @Test
    void getTotalAttributes_afterRangerLevelUp_shouldBeRangerBaseAttributesPlusLevelAttributes() {
        // Arrange
        String expected = "2, 12, 2";

        // Act
        Ranger testCharacter = new Ranger("Test character");
        testCharacter.levelUp();
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether Rogue gains the correct attributes on levelup
     */
    @Test
    void getTotalAttributes_afterRogueLevelUp_shouldBeRogueBaseAttributesPlusLevelAttributes() {
        // Arrange
        String expected = "3, 10, 2";

        // Act
        Rogue testCharacter = new Rogue("Test character");
        testCharacter.levelUp();
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether Warrior gains the correct attributes on levelup
     */
    @Test
    void getTotalAttributes_afterWarriorLevelUp_shouldBeWarriorBaseAttributesPlusLevelAttributes() {
        // Arrange
        String expected = "8, 4, 2";

        // Act
        Warrior testCharacter = new Warrior("Test character");
        testCharacter.levelUp();
        PrimaryAttribute attributes = testCharacter.getTotalAttributes();
        String actual = "" + attributes.getStrength() + ", " +
                attributes.getDexterity() + ", " + attributes.getIntelligence();

        // Assert
        assertEquals(expected, actual);
    }
}