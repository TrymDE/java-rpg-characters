package no.noroff.accelerate.items;

import no.noroff.accelerate.characters.PrimaryAttribute;
import no.noroff.accelerate.characters.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    /**
     * Tests whether we get an InvalidWeaponException with a "level requirement too high" message when the
     * level requirement of a weapon is too high
     */
    @Test
    public void equipItem_weaponLevelTooHigh_shouldThrowInvalidWeaponExceptionWithProperMessage() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Weapon testWeapon = new Weapon("Common Axe", 2, 7, 1.1, WeaponType.AXE);
        String expected = "Weapon level requirement is too high";

        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testCharacter.equipItem(testWeapon));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether we get an InvalidArmorException with a "level requirement too high" message when the
     * level requirement of armor is too high
     */
    @Test
    public void equipItem_armorLevelTooHigh_shouldThrowInvalidArmorExceptionWithProperMessage() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Armor testPlateBody = new Armor("Common Plate Body Armor", 2,
                ItemSlot.BODY, ArmorType.PLATE, new PrimaryAttribute(1, 0, 0));
        String expected = "Armor level requirement is too high";

        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testCharacter.equipItem(testPlateBody));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether we get an InvalidWeaponException with an "invalid type" message when the weapon
     * is of a type the class can't use
     */
    @Test
    public void equipItem_weaponInvalidType_shouldThrowInvalidWeaponExceptionWithProperMessage() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Weapon testBow = new Weapon("Common Bow", 1, 12, 0.8, WeaponType.BOW);
        String expected = "Warriors can only use axes, hammers or swords as weapons";

        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testCharacter.equipItem(testBow));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether we get an InvalidArmorException with an "invalid type" message when the armor
     * is of a type the class can't use
     */
    @Test
    public void equipItem_armorInvalidType_shouldThrowInvalidArmorExceptionWithProperMessage() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Armor testClothHead = new Armor("Common Cloth Head Armor", 1,
                ItemSlot.HEAD, ArmorType.CLOTH, new PrimaryAttribute(0, 0, 5));
        String expected = "Warriors can only equip mail or plate armor";

        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testCharacter.equipItem(testClothHead));
        String actual = exception.getMessage();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests if a character can equip a valid weapon
     */
    @Test
    public void equipItem_weaponIsValid_shouldReturnTrue() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Weapon testWeapon = new Weapon("Common Axe", 1, 7, 1.1, WeaponType.AXE);
        boolean expected = true;

        // Act
        boolean actual = false;
        try {
            actual = testCharacter.equipItem(testWeapon);
        } catch (Exception e) {
            // Should never get here
        }

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests if a character can equip valid armor
     */
    @Test
    public void equipItem_armorIsValid_shouldReturnTrue() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                ItemSlot.BODY, ArmorType.PLATE, new PrimaryAttribute(1, 0, 0));
        boolean expected = true;

        // Act
        boolean actual = false;
        try {
            actual = testCharacter.equipItem(testPlateBody);
        } catch (Exception e) {
            // Should never get here
        }

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether we get the correct DPS number when no weapon is equipped
     */
    @Test
    public void getDPS_weaponNotEquipped_shouldReturnDPS() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        double expected = 1*(1 + (5 / 100));

        // Act
        double actual = testCharacter.getDPS();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether we get the correct DPS number when a weapon is equipped
     */
    @Test
    public void getDPS_weaponEquipped_shouldReturnDPS() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Weapon testWeapon = new Weapon("Common Axe", 1, 7, 1.1, WeaponType.AXE);
        double expected = (7 * 1.1)*(1 + (5 / 100));
        try {
            testCharacter.equipItem(testWeapon);
        } catch (Exception e) {
            // Should never get here
        }

        // Act
        double actual = testCharacter.getDPS();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Tests whether we get the correct DPS number when a weapon and armor is equipped
     */
    @Test
    public void getDPS_weaponAndArmorEquipped_shouldReturnDPS() {
        // Arrange
        Warrior testCharacter = new Warrior("test character");
        Weapon testWeapon = new Weapon("Common Axe", 1, 7, 1.1, WeaponType.AXE);
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                ItemSlot.BODY, ArmorType.PLATE, new PrimaryAttribute(1, 0, 0));
        double expected = (7 * 1.1) * (1 + ((5+1) / 100));
        try {
            testCharacter.equipItem(testWeapon);
            testCharacter.equipItem(testPlateBody);
        } catch (Exception e) {
            // Should never get here
        }

        // Act
        double actual = testCharacter.getDPS();

        // Assert
        assertEquals(expected, actual);
    }

}