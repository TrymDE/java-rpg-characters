# Java assignment 1: RPG Characters
The first assignment for the backend part of the Java fullstack 
course at Noroff in the Experis Academy program.

In this assignment I had to create a commandline application consisting of 
Characters and Items modeled after a typical RPG setting.
This means that characters should have a class that defines both the
attributes and equippable items of said character. Additionally,
I was required to create custom exceptions for when a character
tried to equip an invalid item, either due to it being of the wrong
typing or of a too high level.

## How to run
There are JUnit tests included in the "tests" folder to check
the functionality of the program. If using IntelliJ, download the repo locally, then right click
on the tests folder and click "Run 'All Tests'" to check all functionality.

## Author
Trym Dammann Ellingsen
