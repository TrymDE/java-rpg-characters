package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.*;

public class Rogue extends Character {
    public Rogue(String name) {
        super(name, new PrimaryAttribute(2, 6, 1));
    }

    /**
     * Increase attributes based on Rogue level up specification
     */
    @Override
    public void levelUp() {
        level++;
        baseAttributes.increaseAttributes(1, 4, 1);
    }

    /**
     * Dexterity is the main attribute of the rogue
     * @return Value of the dexterity attribute
     */
    @Override
    public int getMainAttribute() {
        return getTotalAttributes().getDexterity();
    }

    /**
     * Attempt to equip weapon on the Rogue
     * @param weapon Weapon character wants to attempt to equip
     * @throws InvalidWeaponException Thrown if weapon is not a dagger or sword, or if level requirement is too high
     */
    @Override
    protected void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getType() != WeaponType.DAGGER && weapon.getType() != WeaponType.SWORD)
            throw new InvalidWeaponException("Rogues can only use daggers or swords as weapons");
        if (weapon.getLevelRequirement() > level)
            throw new InvalidWeaponException("Weapon level requirement is too high");
        equippedItems.put(weapon.getEquipSlot(), weapon);
    }

    /**
     * Attempt to equip armor to the Rogue
     * @param armor Armor to attempt to equip
     * @throws InvalidArmorException Thrown if armor is not leather or mail, or if level requirement is too high
     */
    @Override
    protected void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getType() != ArmorType.LEATHER && armor.getType() != ArmorType.MAIL)
            throw new InvalidArmorException("Rogues can only equip mail or leather armor");
        if (armor.getLevelRequirement() > level)
            throw new InvalidArmorException("Armor level requirement is too high");
        equippedItems.put(armor.getEquipSlot(), armor);
    }
}
