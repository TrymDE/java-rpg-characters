package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.*;

import java.util.HashMap;

public abstract class Character {
    protected String name;
    protected int level = 1;
    protected PrimaryAttribute baseAttributes;
    protected HashMap<ItemSlot, Item> equippedItems = new HashMap<>();

    /**
     * Constructor for character
     * @param name This is the name of the character. Required for all characters.
     * @param startingAttributes The starting attributes of the character. Varies depending on class.
     */
    public Character(String name, PrimaryAttribute startingAttributes) {
        this.name = name;
        baseAttributes = startingAttributes;
    }

    public int getLevel() {
        return level;
    }

    /**
     * Method for increasing level of a Character. Should always raise level by 1 and
     * increase attributes according to the subclass level form.
     */
    public abstract void levelUp();

    /**
     * Getter for character DPS
     * @return The DPS of the character, a double increased by weapon DPS and character main damage attribute
     */
    public double getDPS(){
        Weapon weapon = (Weapon) equippedItems.get(ItemSlot.WEAPON);
        double weaponDPS = 1;
        if (weapon != null) {
            weaponDPS = weapon.getDPS();
        }
        return weaponDPS * (1 + (getMainAttribute() / 100));
    }

    /**
     * Gets the value of the main attribute for the character. Strength for warrior, etc.
     * Mainly useful when calculating DPS
     * @return The value of the characters main damage attribute.
     */
    public abstract int getMainAttribute();

    /**
     * Gets all attributes of character, both base attributes and from items.
     * @return A PrimaryAttribute object containing the collected attributes of the character including gear.
     */
    public PrimaryAttribute getTotalAttributes() {
        PrimaryAttribute attributes = baseAttributes.cloneAttributes();

        for (ItemSlot slot : equippedItems.keySet()) {
            Item item = equippedItems.get(slot);
            if (item.getEquipSlot() != ItemSlot.WEAPON) {
                Armor armor = (Armor) item;
                PrimaryAttribute armorAttributes = armor.getAttributes();
                attributes.increaseAttributes(armorAttributes.getStrength(),
                        armorAttributes.getDexterity(), armorAttributes.getIntelligence());
            }
        }

        return attributes;
    }

    /**
     * Attempt to equip an Item on the character
     * @param item The item to equip
     * @return True if equip was successful, exception if not
     * @throws Exception Either an InvalidWeaponException or InvalidArmorException
     */
    public boolean equipItem(Item item) throws Exception {
        if (item.getEquipSlot() == ItemSlot.WEAPON)
            equipWeapon((Weapon) item);
        else
            equipArmor((Armor) item);
        return true;
    }

    /**
     * Print all character stats to console, a makeshift character sheet
     */
    public void displayStats() {
        StringBuilder stats = new StringBuilder("Name: ");
        PrimaryAttribute totalAttributes = getTotalAttributes();
        stats.append(name);
        stats.append("\nLevel: ");
        stats.append(level);
        stats.append("\nStrength: ");
        stats.append(totalAttributes.getStrength());
        stats.append("\nDexterity: ");
        stats.append(totalAttributes.getDexterity());
        stats.append("\nIntelligence: ");
        stats.append(totalAttributes.getIntelligence());
        stats.append("\nDPS: ");
        stats.append(getDPS());

        System.out.println(stats);
    }

    /**
     * Individual for all subclasses. Attempts to equip a weapon to character
     * @param weapon Weapon character wants to attempt to equip
     * @throws InvalidWeaponException Thrown if the weapon is wrong type or if the level requirement
     * is higher than the character level
     */
    protected abstract void equipWeapon(Weapon weapon) throws InvalidWeaponException;

    /**
     * Individual for subclasses. Attempts to equip armor to a character.
     * @param armor Armor to attempt to equip
     * @throws InvalidArmorException Thrown if armor type is invalid or if the level requirement
     * is higher than the character level
     */
    protected abstract void equipArmor(Armor armor) throws InvalidArmorException;
}
