package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.*;

public class Warrior extends Character {
    public Warrior(String name) {
        super(name, new PrimaryAttribute(5, 2, 1));
    }

    /**
     * Increase level by one and attributes by Warrior level up specification
     */
    @Override
    public void levelUp() {
        level++;
        baseAttributes.increaseAttributes(3, 2, 1);
    }

    /**
     * Strength is the main attribute of the Warrior
     * @return Value of the Strength attribute.
     */
    @Override
    public int getMainAttribute() {
        return getTotalAttributes().getStrength();
    }

    /**
     * Try to equip a weapon to the Warrior
     * @param weapon Weapon character wants to attempt to equip
     * @throws InvalidWeaponException Thrown if weapon isn't an axe, hammer or sword, or if level requirement is too high
     */
    @Override
    protected void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getType() != WeaponType.AXE && weapon.getType() != WeaponType.HAMMER &&
                weapon.getType() != WeaponType.SWORD)
            throw new InvalidWeaponException("Warriors can only use axes, hammers or swords as weapons");
        if (weapon.getLevelRequirement() > level)
            throw new InvalidWeaponException("Weapon level requirement is too high");
        equippedItems.put(weapon.getEquipSlot(), weapon);
    }

    /**
     * Try to equip armor to the Warrior
     * @param armor Armor to attempt to equip
     * @throws InvalidArmorException Thrown if armor isn't mail or plate, or if level requirement is too high
     */
    @Override
    protected void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getType() != ArmorType.MAIL && armor.getType() != ArmorType.PLATE)
            throw new InvalidArmorException("Warriors can only equip mail or plate armor");
        if (armor.getLevelRequirement() > level)
            throw new InvalidArmorException("Armor level requirement is too high");
        equippedItems.put(armor.getEquipSlot(), armor);
    }
}
