package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.*;

public class Ranger extends Character{
    public Ranger(String name) {
        super(name, new PrimaryAttribute(1, 7, 1));
    }

    /**
     * Increase level by one and attributes by amount specified for Ranger in the task specification
     */
    @Override
    public void levelUp() {
        level++;
        baseAttributes.increaseAttributes(1, 5, 1);
    }

    /**
     * Dexterity is the main attribute of the Ranger
     * @return Value of dexterity attribute
     */
    @Override
    public int getMainAttribute() {
        return getTotalAttributes().getDexterity();
    }

    /**
     * Try to equip weapon to Ranger
     * @param weapon Weapon character wants to attempt to equip
     * @throws InvalidWeaponException Thrown if weapon is not of type bow or if level requirement is too high
     */
    @Override
    protected void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getType() != WeaponType.BOW)
            throw new InvalidWeaponException("Rangers can only use bows as weapons");
        if (weapon.getLevelRequirement() > level)
            throw new InvalidWeaponException("Weapon level requirement is too high");
        equippedItems.put(weapon.getEquipSlot(), weapon);
    }

    /**
     * Try to equip armor to Ranger
     * @param armor Armor to attempt to equip
     * @throws InvalidArmorException Thrown if armor is not of type leather or mail, or if level requirement is too high.
     */
    @Override
    protected void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getType() != ArmorType.LEATHER && armor.getType() != ArmorType.MAIL)
            throw new InvalidArmorException("Rangers can only equip mail or leather armor");
        if (armor.getLevelRequirement() > level)
            throw new InvalidArmorException("Armor level requirement is too high");
        equippedItems.put(armor.getEquipSlot(), armor);
    }
}
