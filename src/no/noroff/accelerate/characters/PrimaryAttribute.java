package no.noroff.accelerate.characters;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Increase the attributes stored in the object by a given amount. Makes it so we don't have to
     * manipulate the internal attribute variables directly, giving us the ability to keep them private.s
     * @param strength Value to increase Strength attribute by
     * @param dexterity Value to increase Dexterity attribute by
     * @param intelligence Value to increase Intelligence attribute by
     */
    public void increaseAttributes(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Method for copying the attributes contained in the object, giving us the ability to manipulate them
     * without actually changing the original attributes. Useful for separating baseAttributes and totalAttributes
     * @return A copy of the current PrimaryAttribute object.
     */
    public PrimaryAttribute cloneAttributes() {
        return new PrimaryAttribute(strength, dexterity, intelligence);
    }
}
