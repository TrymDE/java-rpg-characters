package no.noroff.accelerate.characters;

import no.noroff.accelerate.items.*;

public class Mage extends Character{

    public Mage(String name) {
        super(name, new PrimaryAttribute(1, 1, 8));
    }

    /**
     * Increase attributes by set amount, based on class. Attributes below are for a Mages
     */
    @Override
    public void levelUp() {
        level++;
        baseAttributes.increaseAttributes(1, 1, 5);
    }

    /**
     * Intelligence is the main attribute of a Mage
     * @return Value of the intelligence attribute.
     */
    @Override
    public int getMainAttribute() {
        return getTotalAttributes().getIntelligence();
    }

    /**
     * Attempts to equip a weapon. Fails if the Mage can't equip the given weapon.
     * @param weapon Weapon character wants to attempt to equip
     * @throws InvalidWeaponException Thrown if weapon isn't staff or wand, the weapon types a mage can equip,
     * or if level requirement is too high
     */
    @Override
    protected void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getType() != WeaponType.STAFF && weapon.getType() != WeaponType.WAND)
            throw new InvalidWeaponException("Mages can only use staffs or wands as weapons");
        if (weapon.getLevelRequirement() > level)
            throw new InvalidWeaponException("Weapon level requirement is too high");
        equippedItems.put(weapon.getEquipSlot(), weapon);
    }

    /**
     * Try to equip an armor piece to the Mage
     * @param armor Armor to attempt to equip
     * @throws InvalidArmorException Thrown if the armor is not cloth or if level requirement is too high
     */
    @Override
    protected void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getType() != ArmorType.CLOTH)
            throw new InvalidArmorException("Mages can only equip cloth armor");
        if (armor.getLevelRequirement() > level)
            throw new InvalidArmorException("Armor level requirement is too high");
        equippedItems.put(armor.getEquipSlot(), armor);
    }
}
