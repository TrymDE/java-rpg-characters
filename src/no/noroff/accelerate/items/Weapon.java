package no.noroff.accelerate.items;

public class Weapon extends Item {

    WeaponType type;
    int damage;
    double attackSpeed;

    public Weapon(String name, int levelRequirement,
                  int damage, double attackSpeed, WeaponType weaponType) {

        super(name, levelRequirement, ItemSlot.WEAPON);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        type = weaponType;
    }

    public WeaponType getType() {
        return type;
    }

    public double getDPS() {
        return damage * attackSpeed;
    }
}
