package no.noroff.accelerate.items;

public enum ItemSlot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
