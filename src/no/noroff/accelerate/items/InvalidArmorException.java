package no.noroff.accelerate.items;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String errorMessage) {
        super(errorMessage);
    }
}
