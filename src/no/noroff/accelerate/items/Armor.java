package no.noroff.accelerate.items;

import no.noroff.accelerate.characters.PrimaryAttribute;

public class Armor extends Item {

    ArmorType type;
    PrimaryAttribute attributes;

    public Armor(String name, int levelRequirement, ItemSlot equipSlot,
                 ArmorType type, PrimaryAttribute attributes) {
        super(name, levelRequirement, equipSlot);
        this.type = type;
        this.attributes = attributes;
    }

    public ArmorType getType() {
        return type;
    }

    public PrimaryAttribute getAttributes() {
        return attributes;
    }
}
