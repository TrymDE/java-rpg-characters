package no.noroff.accelerate.items;

public abstract class Item {
    private final String name;
    private final int levelRequirement;
    private final ItemSlot equipSlot;

    public Item(String name, int levelRequirement, ItemSlot equipSlot) {
        this.name = name;
        this.levelRequirement = levelRequirement;
        this.equipSlot = equipSlot;
    }

    public ItemSlot getEquipSlot() {
        return equipSlot;
    }

    public int getLevelRequirement() {
        return levelRequirement;
    }

    public String getName() {
        return name;
    }
}
